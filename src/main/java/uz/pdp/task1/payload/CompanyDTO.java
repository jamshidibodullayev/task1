package uz.pdp.task1.payload;

import lombok.Data;
import uz.pdp.task1.entity.Address;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
@Data
public class CompanyDTO {

    private Integer id;

    @NotNull(message = "Siz company nomini kiritmagansiz")
    private String companyName;

    @NotNull(message = "Siz director nomini kiritmagansiz")
    private String directorName;

    @NotNull(message = "Siz addressni kiritmagansiz")
    private Integer addressId;
}
