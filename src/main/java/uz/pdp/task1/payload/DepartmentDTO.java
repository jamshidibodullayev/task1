package uz.pdp.task1.payload;

import lombok.Data;
import uz.pdp.task1.entity.Company;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
@Data
public class DepartmentDTO {
    private Integer id;

    @NotNull(message = "Siz departmentni nomini kiritmagansiz")
    private String name;

    @NotNull(message = "Siz Companiyani kiritmagansiz")
    private Integer companyId;
}
