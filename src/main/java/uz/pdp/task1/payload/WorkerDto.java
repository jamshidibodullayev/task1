package uz.pdp.task1.payload;

import lombok.Data;
import uz.pdp.task1.entity.Address;
import uz.pdp.task1.entity.Department;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class WorkerDto {

    private Integer id;

    @NotNull(message = "Siz ischini nomini kiritmagansiz")
    private String name;

    @Size(min =7, max = 13)
    @NotNull(message = "Siz telifon raqami kiritmagansiz")
    private String phoneNumber;

    @NotNull(message = "Siz address kiritmagansiz")
    private Integer addressId;

    @NotNull(message = "Siz department kiritmagansiz")
    private Integer departmentId;
}
