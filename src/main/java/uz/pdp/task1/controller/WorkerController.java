package uz.pdp.task1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task1.entity.Worker;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.WorkerDto;
import uz.pdp.task1.service.WorkerService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/worker")
public class WorkerController {
    @Autowired
    WorkerService workerService;

    @GetMapping
    public HttpEntity<?> getAllWorker(){
        List<Worker> allWorker = workerService.getAllWorker();
        return ResponseEntity.ok().body(allWorker);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getByIdWorker(@PathVariable Integer id){
        Worker worker= workerService.getByIdWorker(id);
        return ResponseEntity.status(worker!=null?200:404).body(worker!=null?worker:"Siz kiritgan ID lik Worker mavjud emas");
    }

    @PostMapping
    public HttpEntity<?> addWorker(@Valid @RequestBody WorkerDto workerDto){
        ApiResponse apiResponse= workerService.addWorker(workerDto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editWorker(@Valid @RequestBody WorkerDto workerDto, @PathVariable Integer id){
        ApiResponse apiResponse= workerService.editWorker(id, workerDto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteWorker(@PathVariable Integer id){
        ApiResponse apiResponse= workerService.deleteWorker(id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);

    }
}
