package uz.pdp.task1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task1.entity.Company;
import uz.pdp.task1.entity.Department;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.CompanyDTO;
import uz.pdp.task1.payload.DepartmentDTO;
import uz.pdp.task1.service.CompanyService;
import uz.pdp.task1.service.DepartmentService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/department")
public class DepartmentController {
    @Autowired
    DepartmentService departmentService;

    @GetMapping
    public HttpEntity<?> getAllCompany(){
        List<Department> companyList=departmentService.getAllDeparment();
        return ResponseEntity.status(200).body(companyList.size()!=0?companyList:"Departmentlar mavjud emas.");
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getByIdCompany(@PathVariable Integer id){
        Department department= departmentService.getByIdDeparment(id);
        return ResponseEntity.status(department!=null?200:409).body(department!=null?department:"Siz kiritgan ID lik Department mavjud emas.");
    }

    @PostMapping
    public HttpEntity<?> addCompany(@Valid @RequestBody DepartmentDTO departmentDTO){
        ApiResponse apiResponse= departmentService.addDeparment(departmentDTO);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editCompany(@PathVariable Integer id, @Valid @RequestBody DepartmentDTO departmentDTO){
        ApiResponse apiResponse= departmentService.editDeparment(id,departmentDTO);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteCompany(@PathVariable Integer id){
        ApiResponse apiResponse= departmentService.deleteDeparment(id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> xatolikniUshlabOlibOzimizniMessageniQaytarish(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
