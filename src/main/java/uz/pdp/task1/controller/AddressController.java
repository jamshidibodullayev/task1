package uz.pdp.task1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task1.entity.Address;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.service.AddressService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/address")
public class AddressController {
    @Autowired
    AddressService addressService;

    @GetMapping
    public HttpEntity<?> getAllAddress(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size){
       List<Address> addressList=addressService.getAllAddress(page, size);
       return ResponseEntity.status(addressList.size()!=0?200:409).body(addressList.size()!=0?addressList:"Addresslar mavjus emas");
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getByIdAddress(@PathVariable Integer id){
       Address address=addressService.getByIdAddress(id);
        return ResponseEntity.status(address!=null?200:409).body(address!=null?address:"Siz kiritgan Id lik addres mavjud emas");
    }

    @PostMapping
    public HttpEntity<?> addAddress(@Valid @RequestBody Address address){
        ApiResponse apiResponse=addressService.addAddress(address);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editAddress(@PathVariable Integer id, @Valid @RequestBody Address address){
        ApiResponse apiResponse=addressService.editAddres(id, address);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);

    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteAddress(@PathVariable Integer id){
        ApiResponse apiResponse=addressService.deleteAddress(id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }






    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> xatolikniUshlabOlibOzimizniMessageniQaytarish(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }


}
