package uz.pdp.task1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import uz.pdp.task1.entity.Address;
import uz.pdp.task1.entity.Company;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.CompanyDTO;
import uz.pdp.task1.repository.AddressRepository;
import uz.pdp.task1.repository.CompanyRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {
    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    AddressRepository addressRepository;

    public List<Company> getAllCompany(){
        return companyRepository.findAll();
    }

    public Company getByIdCompany(Integer id){
        return companyRepository.findById(id).orElse(null);
    }

    public ApiResponse addCompany(CompanyDTO companyDTO){
        Optional<Address> optionalAddress = addressRepository.findById(companyDTO.getAddressId());
        if (!optionalAddress.isPresent()){
            return new ApiResponse(false, "Siz kiritgan Id lik address mavjud emas");
        }

        Company company=new Company(companyDTO.getCompanyName(), companyDTO.getDirectorName(), optionalAddress.get());
        try {
            companyRepository.save(company);
            return new ApiResponse(true, "Company saqlandi");
        }catch (Exception e){
         return new ApiResponse(false, "Siz kiritgan "+companyDTO.getCompanyName()+" nomli Company mavjud");
        }

    }

    public ApiResponse editCompany(Integer id, CompanyDTO companyDTO){
        Optional<Company> optionalCompany = companyRepository.findById(id);
        if (!optionalCompany.isPresent()){
            return new ApiResponse(false, "Siz kiritgan Id lik Company mavjud emas");
        }

        Optional<Address> optionalAddress = addressRepository.findById(companyDTO.getAddressId());
        if (!optionalAddress.isPresent()){
            return new ApiResponse(false, "Siz kiritgan Id lik address mavjud emas");
        }

        Company company=new Company(id, companyDTO.getCompanyName()!=null? companyDTO.getCompanyName():optionalCompany.get().getCompanyName(),
                companyDTO.getDirectorName()!=null?companyDTO.getDirectorName():optionalCompany.get().getDirectorName(),
                optionalAddress.get());
        try {
            companyRepository.save(company);
            return new ApiResponse(true, "Company tahrirlandi");
        }catch (Exception e){
            return new ApiResponse(false, "Siz kiritgan "+companyDTO.getCompanyName()+" nomli Company mavjud");
        }
    }

    public ApiResponse deleteCompany(Integer id){
        Optional<Company> optionalCompany = companyRepository.findById(id);
        if (!optionalCompany.isPresent()){
            return new ApiResponse(false, "Siz kiritgan Id lik Company mavjud emas");
        }
        try {
            companyRepository.deleteById(id);
            addressRepository.deleteById(optionalCompany.get().getAddress().getId());
           return new ApiResponse(true, "Muvaffiyaqatli o`chirildi.");
        }catch (Exception e){
            return new ApiResponse(false, "Company Forign key orqali ulangan.");
        }

    }

}
