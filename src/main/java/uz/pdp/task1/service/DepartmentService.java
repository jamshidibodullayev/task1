package uz.pdp.task1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task1.entity.Company;
import uz.pdp.task1.entity.Department;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.DepartmentDTO;
import uz.pdp.task1.repository.CompanyRepository;
import uz.pdp.task1.repository.DepartmentRepository;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService {
    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    public List<Department> getAllDeparment(){
        return departmentRepository.findAll();
    }

    public Department getByIdDeparment(Integer id){
        return departmentRepository.findById(id).orElse(null);
    }

    public ApiResponse addDeparment(DepartmentDTO departmentDTO){
        Optional<Company> optionalCompany = companyRepository.findById(departmentDTO.getCompanyId());
        if (!optionalCompany.isPresent()){
            return new ApiResponse(false, "Siz kiritgan Id lik Company mavjud emas");
        }

        Department department=new Department(departmentDTO.getName(), optionalCompany.get());
        try {
            departmentRepository.save(department);
            return new ApiResponse(true, "Siz kiritgan "+departmentDTO.getName()+ " nomli department saqlandi");
        }catch (Exception e){
         return new ApiResponse(false, "Siz kiritgan "+departmentDTO.getName()+" nomli department mavjud siz tanlagan "+
                 optionalCompany.get().getCompanyName() +" nomli Companyda mavjud.");
        }

    }

    public ApiResponse editDeparment(Integer id, DepartmentDTO departmentDTO){
        Optional<Department> optionalDepartment = departmentRepository.findById(id);
        if (!optionalDepartment.isPresent()){
            return new ApiResponse(false, "Siz kiritgan Id lik Department mavjud emas");
        }

        Optional<Company> optionalCompany = companyRepository.findById(departmentDTO.getCompanyId());
        if (!optionalCompany.isPresent()){
            return new ApiResponse(false, "Siz kiritgan Id lik address mavjud emas");
        }

       Department department=new Department(id, departmentDTO.getName(), optionalCompany.get());

        try {
            departmentRepository.save(department);
            return new ApiResponse(true, "Department tahrirlandi");
        }catch (Exception e){
            return new ApiResponse(false, "Siz kiritgan "+departmentDTO.getName()+" nomli Department " +
                    optionalCompany.get().getCompanyName()+"da mavjud");
        }
    }

    public ApiResponse deleteDeparment(Integer id){
        Optional<Department> optionalDepartment = departmentRepository.findById(id);
        if (!optionalDepartment.isPresent()){
            return new ApiResponse(false, "Siz kiritgan Id lik Department mavjud emas");
        }
        try {
           departmentRepository.deleteById(id);
           return new ApiResponse(true, "Muvaffiyaqatli o`chirildi.");
        }catch (Exception e){
            return new ApiResponse(false, "Deparment Forign key orqali ulangan.");
        }

    }

}
