package uz.pdp.task1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.task1.entity.Address;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.repository.AddressRepository;

import java.util.List;
import java.util.Optional;

@Service
public class AddressService {
    @Autowired
    AddressRepository addressRepository;

    public List<Address> getAllAddress(int page, int size) {
        Pageable pageable= PageRequest.of(page, size);
        Page<Address> allAddress =addressRepository.findAll(pageable);
        return allAddress.getContent();
    }


    public Address getByIdAddress(Integer id) {
    return addressRepository.findById(id).orElse(null);
    }


    public ApiResponse addAddress(Address address) {
        Address saveAddress = addressRepository.save(address);
        return new ApiResponse(true, "Address saqlandi");
    }

    public ApiResponse editAddres(Integer id, Address address) {
        Optional<Address> optionalAddress = addressRepository.findById(id);
        if (!optionalAddress.isPresent()){
            return new ApiResponse(false, "Siz kiritgan Id lik Address mavjud emas.");
        }
        optionalAddress.get().setStreet(address.getStreet());
        optionalAddress.get().setHomeNumber(address.getHomeNumber());
        addressRepository.save(optionalAddress.get());
        return new ApiResponse(true, "Adddress tahrirlandi");
    }


    public ApiResponse deleteAddress(Integer id) {
        Optional<Address> optionalAddress = addressRepository.findById(id);
        if (optionalAddress.isPresent()){
        try {
            addressRepository.deleteById(id);
        }catch (Exception e){
            return new ApiResponse(false, "Siz kiritgan Id lik address forign key orqali bog`langan");
        }
    }            return new ApiResponse(false, "Siz kiritgan Id lik mavjud emas");

    }
}
