package uz.pdp.task1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task1.entity.Address;
import uz.pdp.task1.entity.Department;
import uz.pdp.task1.entity.Worker;
import uz.pdp.task1.payload.ApiResponse;
import uz.pdp.task1.payload.WorkerDto;
import uz.pdp.task1.repository.AddressRepository;
import uz.pdp.task1.repository.DepartmentRepository;
import uz.pdp.task1.repository.WorkerRepository;

import java.util.List;
import java.util.Optional;

@Service
public class WorkerService {
    @Autowired
    WorkerRepository workerRepository;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    public List<Worker> getAllWorker(){
        return workerRepository.findAll();
    }

    public Worker getByIdWorker(Integer id){
        return workerRepository.findById(id).orElse(null);
    }

    public ApiResponse addWorker(WorkerDto workerDto){
        Optional<Address> optionalAddress = addressRepository.findById(workerDto.getAddressId());
        if (optionalAddress.isPresent()){
            return new ApiResponse(false, "Siz address kiritmagansiz");
        }

        Optional<Department> optionalDepartment = departmentRepository.findById(workerDto.getDepartmentId());
        if (optionalDepartment.isPresent()){
            return new ApiResponse(false, "Siz deparment kiritmagansiz");
        }

        Worker worker=new Worker(workerDto.getName(), workerDto.getPhoneNumber(),
                optionalAddress.get(), optionalDepartment.get());
        try {
            Worker saveWorker = workerRepository.save(worker);
            return new ApiResponse(false, "Worker saqlandi. ID: "+saveWorker.getId());
        }catch (Exception e){
            return new ApiResponse(false, "Siz kiritgan telifon raqamli ishchi mavjud ");
        }
    }

    public ApiResponse deleteWorker(Integer id){
        Optional<Worker> optionalWorker = workerRepository.findById(id);
        if (!optionalWorker.isPresent()){
            return new ApiResponse(false, "Siz kiritgan Id lik worker mavjud emas.");
        }
        try {
            workerRepository.deleteById(id);
            addressRepository.deleteById(optionalWorker.get().getAddress().getId());
            return new ApiResponse(true, "Siz kiritgan Id lik worker o`chirildi");
        }catch (Exception e){
            return new ApiResponse(false, "Siz kiritgan Id lik worker o`chirilmadi.");
        }
    }

    public ApiResponse editWorker(Integer id, WorkerDto workerDto) {
        Optional<Worker> optionalWorker = workerRepository.findById(id);
        if (optionalWorker.isPresent()){
            return new ApiResponse(true, "Siz kiritgan id lik Worker mavjud emas");
        }
        Optional<Address> optionalAddress = addressRepository.findById(workerDto.getAddressId());
        if (optionalAddress.isPresent()){
            return new ApiResponse(false, "Siz address kiritmagansiz");
        }

        Optional<Department> optionalDepartment = departmentRepository.findById(workerDto.getDepartmentId());
        if (optionalDepartment.isPresent()){
            return new ApiResponse(false, "Siz deparment kiritmagansiz");
        }

        Worker worker=new Worker(id,workerDto.getName(), workerDto.getPhoneNumber(),
                optionalAddress.get(), optionalDepartment.get());
        try {
            Worker saveWorker = workerRepository.save(worker);
            return new ApiResponse(false, "Worker saqlandi. ID: "+saveWorker.getId());
        }catch (Exception e){
            return new ApiResponse(false, "Siz kiritgan telifon raqamli ishchi mavjud ");
        }
    }

}
