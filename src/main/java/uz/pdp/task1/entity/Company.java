package uz.pdp.task1.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true, nullable = false)
    private String companyName;

    @Column(nullable = false)
    private String directorName;

    @OneToOne @JoinColumn(nullable = false)
    private Address address;

    public Company(String companyName, String directorName, Address address) {
        this.companyName = companyName;
        this.directorName = directorName;
        this.address = address;
    }
}
